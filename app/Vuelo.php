<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vuelo extends Model
{
    protected $fillable = [
        'fecha_inicio',
        'fecha_fin',
        'tiempo_vuelo',
        'hora_llegada',
        'ciudad_salida',
        'ciudad_destino',
        'aerolinea_id',
    ];

    public $timestamps = false;
}
