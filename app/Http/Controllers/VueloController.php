<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vuelo;

class VueloController extends Controller
{

    public function index()
    {
        return response()->json(Vuelo::all());
    }

}
