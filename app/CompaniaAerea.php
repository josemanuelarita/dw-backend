<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompaniaAerea extends Model
{

    protected $fillable = ['nombre', 'ciudad'];
    public $timestamps = false;

}
