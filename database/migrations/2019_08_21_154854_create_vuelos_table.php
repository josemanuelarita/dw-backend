<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVuelosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vuelos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fecha_inicio');
            $table->string('fecha_fin');
            $table->string('tiempo_vuelo');
            $table->string('hora_llegada');
            $table->string('ciudad_salida');
            $table->string('ciudad_destino');
            $table->unsignedBigInteger('aerolinea_id');
            $table->timestamps();

            $table->foreign('aerolinea_id')->references('id')->on('compania_aereas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vuelos');
    }
}
